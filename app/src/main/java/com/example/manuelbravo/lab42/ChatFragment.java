package com.example.manuelbravo.lab42;

import android.support.v4.app.Fragment;


import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.os.Handler;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;




public class ChatFragment extends Fragment{

    private final static int LOGIN_INTENT = 1;

    private Handler handler;
    private Runnable runnable;
    private DatabaseReference fireDb;

    private ListView messages;
    private Button buttonChat;
    private EditText editTextChat;
    private TextView textViewChat;

    private final String userPrefFile = "userpref";

    private String userName;
    private ArrayList<String> messageList;
    private ArrayAdapter<String> adapter;

    private boolean notify = false;
    private boolean activityBackGround;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.chat_fragment, container, false);

        //Get intance of DB
        fireDb = FirebaseDatabase.getInstance().getReference();

        //Get Button, TextView, Listview...
        buttonChat = (Button) view.findViewById(R.id.button_chat);
        textViewChat = (TextView) view.findViewById(R.id.textView_chat);
        editTextChat = (EditText) view.findViewById(R.id.editText_chat);
        messages = (ListView) view.findViewById(R.id.listView_chat);

        //Associates  ListView with the adapter.
        messageList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, messageList);
        messages.setAdapter(adapter);


        userInit();
        fireDb.child("msg").addChildEventListener(new OnChatListener());

        //Send a new message
        buttonChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        handler = new Handler();
        runnable = new Runnable()
        {
            @Override
            public void run()
            {
                new notifyInBackGround().execute((Void) null);
                handler.postDelayed(runnable, 5000);
            }
        };
        handler.post(runnable);

        return view;
    }



    private class OnChatListener implements ChildEventListener{

        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            String getMessage = dataSnapshot.child("m").getValue(String.class);
            messageList.add(getMessage);
            adapter.notifyDataSetChanged();
            messages.setSelection(adapter.getCount() - 1);
            if((userName != null) &&(!userName.matches(dataSnapshot.child("u").getValue(String.class))))
                if(activityBackGround) notify = true;
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if((requestCode == LOGIN_INTENT) && (resultCode == getActivity().RESULT_OK)){

            userName = data.getStringExtra("USR");
            textViewChat.setText(userName);

            SharedPreferences userPref = getActivity().getSharedPreferences(
                    userPrefFile,
                    getActivity().MODE_PRIVATE
            );
            SharedPreferences.Editor userEdit = userPref.edit();
            userEdit.putString("USERNAME", userName);
            userEdit.apply();
            fireDb.child("users").push().setValue(userName);
        }
    }


    /**
     * Sends a message and save it in the Firebase DB
     */
    private void sendMessage(){

        String msg;
        msg = editTextChat.getText().toString();
        if( (msg.length() <= 255) && !(msg.matches("")) ) {
            msg = userName + " : " + msg;
            Map<String, String> map = new HashMap<>();
            map.put("u", userName);
            map.put("m", msg);
            // put date ("d");

            fireDb.child("msg").push().setValue(map);
            hideKeyboard(getActivity());
            adapter.notifyDataSetChanged();
        }

        editTextChat.setText("");

    }

    /**
     * Reads Share Preferences in order to know if there is a user stored
     *
     */
    private void userInit(){

        //Read Shared Preferences.
        SharedPreferences userPref = getActivity().getSharedPreferences(userPrefFile, getActivity().MODE_PRIVATE);
        String name = userPref.getString("USERNAME", null);
        userName = name;

        //A new user must be created if a user is not found.
        if( (userName == null) || (userName.isEmpty()) ) {
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivityForResult(intent, LOGIN_INTENT);
        }
        textViewChat.setText(userName);

    }


    /**
     * Hides the keyboard in the View
     *
     * @param activity
     */
    private static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    private class notifyInBackGround extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if(notify){
                notificationCall();

            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(success){
                super.onPostExecute(success);


            }
        }
    }

    /**
     * Shows notification when new messages
     *
     */
    private void notificationCall(){
        NotificationCompat.Builder notificationBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(getContext())
                        .setDefaults(NotificationCompat.DEFAULT_ALL)
                        .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                        .setContentTitle("New message");

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());

        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pend = PendingIntent.getActivity(
                getActivity(),
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setContentIntent(pend);

        notificationManager.notify(0, notificationBuilder.build());
        notify = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        activityBackGround = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        activityBackGround = true;
    }

}


