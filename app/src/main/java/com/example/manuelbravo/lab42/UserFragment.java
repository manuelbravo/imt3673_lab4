package com.example.manuelbravo.lab42;


import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.content.Intent;

import android.widget.AdapterView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.Collections;

/**
 * Created by manuelbravo on 03.04.2018.
 */

public class UserFragment extends Fragment {

    private ListView listView;
    private ArrayList<String> userList;
    private ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_fragment, container, false);

        DatabaseReference fireDb;
        fireDb = FirebaseDatabase.getInstance().getReference();
        listView = (ListView) view.findViewById(R.id.userlist);

        userList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                userList
        );

        listView.setAdapter(adapter);
        fireDb.child("users").addChildEventListener(new OnUserListener());
        listView.setOnItemClickListener(new ItemListener());
        return view;
    }


    /**
     * If we select an user of the listview
     * We will call to the UserOwnMsgActivity
     * That will show the messeges of the selected user.
     *
     */
    private class ItemListener implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id)
        {

            Intent intent = new Intent (getActivity(), UserOwnMsgActivity.class);
            intent.putExtra("SELECTEDUSER", userList.get(position));
            startActivity (intent);
        }
    }

    /**
     * Updates the user listview
     */
    private class OnUserListener implements ChildEventListener{
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            String retrievedUser = dataSnapshot.getValue(String.class);
            userList.add(retrievedUser);
            Collections.sort(userList, String.CASE_INSENSITIVE_ORDER);
            adapter.notifyDataSetChanged();
            listView.setSelection(adapter.getCount() - 1);
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
