package com.example.manuelbravo.lab42;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.Random;



public class LoginActivity extends AppCompatActivity {

    private static final int MIN_USERNAME_LENGHT = 5;
    private static final int MAX_USERNAME_LENGTH = 8;
    private String userName;
    private EditText editTextUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button button = (Button) findViewById(R.id.button_newUser);
        editTextUser = (EditText) findViewById(R.id.editText_newUser);

        //random = new Random();
        String generatedName = createUsername();

        editTextUser.setText(generatedName);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = editTextUser.getText().toString();
                Intent intent = new Intent();
                intent.putExtra("USR", userName);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }


    /**
     * https://stackoverflow.com/questions/12116092/android-random-string-generator
     * @return random string
     */
    private String createUsername() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        // gets string length in range MIN - MAX
        int randomLength = generator.nextInt(MAX_USERNAME_LENGTH - MIN_USERNAME_LENGHT + 1) + MIN_USERNAME_LENGHT;
        char tempChar;
        tempChar = (char) (generator.nextInt(25) + 65);
        randomStringBuilder.append(tempChar);

        for (int i = 1; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(25) + 97);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }





}


