package com.example.manuelbravo.lab42;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;

import android.support.v4.view.ViewPager;
import android.util.Log;

import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    private FirebaseAuth fireAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializes Firebase And Tries to Sign in Anonymously.
        FirebaseApp.initializeApp(this);
        this.fireAuth = FirebaseAuth.getInstance();
        signInAnonymously();


         //Initializes Tabs and their fragments.
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_container);
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(new ChatFragment(), "Messages");
        adapter.addFragment(new UserFragment(), "Users");
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
    }


    /**
     * Used to sign in anonymously in Firebase DB
     * From Firebase documentation
     */
    private void signInAnonymously() {

        this.fireAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("From MainActivity", "signInAnonymously:success");
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Froom MainActivity", "signInAnonymously:fail", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }


}




