package com.example.manuelbravo.lab42;

import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import android.content.Intent;

import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;


public class UserOwnMsgActivity extends AppCompatActivity {

    private ArrayList<String> messageList;
    private ArrayAdapter<String> adapter;
    private String username;
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usr_msg);
        DatabaseReference mDb;
        mDb = FirebaseDatabase.getInstance().getReference();


        Intent intent = getIntent();
        username = intent.getStringExtra("SELECTEDUSER");


        listView = (ListView) findViewById(R.id.listView_usr_msg);
        messageList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                messageList
        );
        listView.setAdapter(adapter);

        mDb.child("msg").addChildEventListener(new UserMessagesListener());
    }


    /**
     * Show the messages of an certain user
     */
    private class UserMessagesListener implements ChildEventListener{
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if(username.matches(dataSnapshot.child("u").getValue(String.class))) {
                String getMessage = dataSnapshot.child("m").getValue(String.class);
                messageList.add(getMessage);
                adapter.notifyDataSetChanged();
                listView.setSelection(adapter.getCount() - 1);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }


        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }
}
